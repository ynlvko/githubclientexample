package com.example.githubclientexample.screens;

import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {
    protected final String mTag;

    public BaseFragment() {
        mTag = getClass().getSimpleName();
    }

    protected View inflate(LayoutInflater inflater, @LayoutRes int resource, @Nullable ViewGroup container) {
        View v = inflater.inflate(resource, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    protected void logI(String msg) {
        Log.i(mTag, msg);
    }

    protected void logD(String msg) {
        Log.d(mTag, msg);
    }

    protected void logE(String msg) {
        Log.e(mTag, msg);
    }
}
