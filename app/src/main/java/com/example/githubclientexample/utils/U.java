package com.example.githubclientexample.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Common helper (utils)
 */
public class U {

    public static JsonObject getErrorFromResposne(Response response) {
        return new JsonParser().parse(new String(((TypedByteArray) response.getBody()).getBytes())).getAsJsonObject();
    }
}
