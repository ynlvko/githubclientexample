package com.example.githubclientexample.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Preferences helper
 */
public class P {
    private static final String PREFS_NAME = "VideoStreamingAppPreferences";

    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_TOKEN   = "token";

    public static String getString(Context context, String key) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(key, null);
    }

    public static String getToken(Context context) {
        return getString(context, PREF_TOKEN);
    }

    public static void put(Context context, String key, String value) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void put(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static boolean isSignedIn(Context context) {
        return !TextUtils.isEmpty(getToken(context));
    }
}
